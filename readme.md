# Как полуавтоматически развернуть такую же инфру:

- настроить yc, докер, s3cmd:
```shell
yc config set token <oauth-token>
yc config set cloud-id <cloud-id>
yc config set folder-id <folder-id>
yc container registry configure-docker
s3cmd --configure
```

- создать руками кластер ydb с необходимыми настройками, посмотреть инфу `yc ydb database list`, найти нужный кластер, 
значение ENDPOINT имеет вид <ENDPOINT>?<DATABASE_NAME>, вставить эти значения в `backend/.env` YDB_ENDPOINT=<ENDPOINT> 
YDB_DATABASE=<DATABASE_NAME>

- в фолдере создать сервисный аккаунт с ролями `ydb.admin`, `ydb.viewer`, `ydb.editor`
У сервисного аккаунта создать авторизованный ключ, там предложит скачать файл с данными ключа, содержимое этого 
файла скопировать в `./backend/ydb_key`

- создать целевую группу и балансировщик:
```shell
source scripts/create balancer.sh`
```

- создать container registry: 
```shell
source scripts/create_registry.sh
```

- руками выдать права пуллить из хранилища

- собрать образы бэка и запушить в хранилище: 
```shell
./scripts/build_backend.sh <version>
```

- развернуть виртуалку:
```shell
./scripts/deploy_backend.sh <version> <path-to-ssh-key-public-part>
```

- создать object storage с нужными правами, настроить веб сайт в бакете

- развернуть фронт:
```shell
./scripts/deploy_frontend.sh <version>
```

# Как загрузить новый образ бэка:
- иметь настроенный yc и докер
- иметь корректные значения в `./backend/.env` и `./backend/ydb_key`

- иметь переменную окружения `REGISTRY_ID` - реестр, в который класть образ
  (`create_registry.sh` добавляет автоматически, в остальных случаях:
```shell
export REGISTRY_ID=<registry-id>
```
)

либо использовать 
```shell
source scripts/set_env.sh
```

- собрать и загрузить новый образ:
```shell
./scripts/build_backend.sh <new-version-tag>
```

## Как добавить реплику бэка:
- иметь настроенный yc

- иметь переменную окружения `REGISTRY_ID` - реестр, из которого брать образ
  (`create_registry.sh` добавляет автоматически, в остальных случаях:
```shell
export REGISTRY_ID=<registry-id>
```
)

либо использовать
```shell
source scripts/set_env.sh
```

- иметь переменную окружения `TARGET_GROUP_ID` - целевая группа, к которой будет подключена реплика
  (`create_balancer.sh` добавляет автоматически, в остальных случаях:
```shell
export TARGET_GROUP_ID=<target-group-id>
```
)

либо использовать
```shell
source scripts/set_env.sh
```

- развернуть реплику:
```shell
./scripts/deploy_backend.sh <version> <path-to-ssh-key-public-part>
```

## Как обновить версию бэка на реплике:
- иметь настроенный yc

- иметь переменную окружения `REGISTRY_ID` - реестр, из которого брать образ
  (`create_registry.sh` добавляет автоматически, в остальных случаях:
```shell
export REGISTRY_ID=<registry-id>
```
)

либо использовать
```shell
source scripts/set_env.sh
```

- обновить версию:
```shell
./scripts/update_replica.sh <instance_id> <new-version-tag>
```

## Как обновить версию фронта:
- иметь настроенный yc и s3cmd

- иметь переменную окружения `LISTENER_ADDRESS` - адрес, по которому будут отправляться запросы на бэкенд 
(`create_balancer.sh` добавляет автоматически, в остальных случаях:
```shell
export LISTENER_ADDRESS=<listener-address>
```
)

либо использовать
```shell
source scripts/set_env.sh
```

- развернуть командой:
```shell
./scripts/deploy_frontend.sh <new-version-tag>
```

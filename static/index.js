const frontVersionElement = document.getElementById("front-version");
frontVersionElement.innerText = `Текущая версия фронтенда: ${FRONT_VERSION}`;

async function renderComments() {
    const commentsList = document.getElementById("comments-list");
    commentsList.innerHTML = "";

    const response = await fetch(`${BACKEND_URL}/comments`, {
        method: "GET",
    });
    const json = await response.json();

    if (json["comments"].length === 0) {
        commentsList.innerHTML = "<h3>Отзывов пока нет</h3>";
    }
    for (const comment of json["comments"]) {
        commentsList.innerHTML += `<li>${comment["name"]} ${comment["surname"]} говорит: ${comment["text"]}</li>`;
    }
    document.getElementById("backend-info").innerText = `Список отзывов отдал бэкенд ${json['whoami']}`;
}

renderComments();

document.getElementById("add-comment-button").addEventListener("click", async () => {
    const name = document.getElementById("name").value;
    const surname = document.getElementById("surname").value;
    const text = document.getElementById("comment-area").value;

    const response = await fetch(`${BACKEND_URL}/comments/new`, {
        method: "POST",
        body: JSON.stringify({
            name: name,
            surname: surname,
            text: text,
        }),
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }
    });
    const json = await response.json();

    document.getElementById("backend-info").innerText = `Отзыв добавил бэкенд ${json['whoami']}`;
    await renderComments();
});

document.getElementById("search-comments-button").addEventListener("click", async () => {
    const commentsList = document.getElementById("surname-comments-list");
    commentsList.innerHTML = "";

    const surname = document.getElementById("surname-search").value;

    const response = await fetch(`${BACKEND_URL}/comments/${surname}`, {
        method: "GET",
    });
    const json = await response.json();

    if (json["comments"].length === 0) {
        commentsList.innerHTML = "<h3>Этот человек не оставлял отзывов</h3>";
    }
    for (const comment of json["comments"]) {
        commentsList.innerHTML += `<li>${comment["name"]} ${comment["surname"]} говорит: ${comment["text"]}</li>`;
    }
    document.getElementById("backend-info").innerText = `Список отзывов отдал бэкенд ${json['whoami']}`;
});

#!/bin/bash

registry_id=$(yc container registry create --name backend-registry | grep "^id: " | awk '{split($0,a,": "); print a[2]}')
export REGISTRY_ID=$registry_id

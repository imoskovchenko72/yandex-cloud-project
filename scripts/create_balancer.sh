#!bin/bash

target_group_id=$(yc load-balancer target-group create backends-group | grep "^id: " | awk '{split($0,a,": "); print a[2]}')
listener_address=$(yc load-balancer network-load-balancer create backend-balancer --target-group target-group-id=$target_group_id,healthcheck-name=backend-healthchek,healthcheck-http-port=8080,healthcheck-http-path=/ping --listener name=backend-listener,port=80,target-port=8080,external-ip-version=ipv4 | grep "address: " | awk '{split($0,a,": "); print a[2]}')

export TARGET_GROUP_ID=$target_group_id
export LISTENER_ADDRESS=$listener_address

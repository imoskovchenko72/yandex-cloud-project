#!/bin/bash

tag=$1
ssh_key_file=$2
registry_id=$(printenv REGISTRY_ID)
target_group_id=$(printenv TARGET_GROUP_ID)

if [ -z "$tag" ]
then
    echo "tag not specified"
    exit 0
fi

if [ -z "$ssh_key_file" ]
then
    echo "path to public part of ssh key not specified"
    exit 0
fi

if [ -z "$registry_id" ]
then
    echo "no env REGISTRY_ID"
    exit 0
fi

if [ -z "$target_group_id" ]
then
    echo "no env TARGET_GROUP_ID"
    exit 0
fi


echo "creating virtual machine with specified docker image"
replica_id=$(yc compute instance create-with-container \
--zone ru-central1-a \
--ssh-key $ssh_key_file \
--platform standard-v3 \
--create-boot-disk size=30 \
--public-ip \
--container-name=backend \
--container-image=cr.yandex/$registry_id/comments-service:$tag \
--container-command=/app/run.sh \
--container-env-file=./backend/.env \
--container-privileged | grep "^id: " | awk '{split($0,a,": "); print a[2]}')

echo "adding virtual machine to balancer target group"
replica_address=$(yc compute instance list | grep $replica_id | awk '{split($0,a,"|"); gsub(/ /, "", a[7]); print a[7]}')

yc load-balancer target-group add-targets --id $target_group_id --target subnet-name=default-ru-central1-a,address=$replica_address

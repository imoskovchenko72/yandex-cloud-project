#!/bin/bash

bucket_name=$(printenv BUCKET_NAME)
tag=$1
backend_url=$(printenv LISTENER_ADDRESS)

if [ -z "$bucket_name" ]
then
    echo "no env BUCKET_NAME"
    exit 0
fi

if [ -z "$tag" ]
then
    echo "frontend tag not specified"
    exit 0
fi

if [ -z "$backend_url" ]
then
    echo "no env LISTENER_ADDRESS"
    exit 0
fi

# create temporary directory to add file with version
cp -r ./static ./tmp_static

# add actual version
echo "const FRONT_VERSION = '$tag'" > ./tmp_static/version.js

# add backend url
echo "const BACKEND_URL = 'http://$backend_url'" > ./tmp_static/constants.js

# push all files of new version
for file in `find ./tmp_static -type f -name "*"`
do
   s3cmd put $file s3://$bucket_name
done

# remove temporary directory
rm -rf ./tmp_static

#!/bin/bash

instance_id=$1
tag=$2
registry_id=$(printenv REGISTRY_ID)

if [ -z "$instance_id" ]
then
    echo "no instance id specified"
    exit 0
fi

if [ -z "$tag" ]
then
    echo "no replica new version specified"
    exit 0
fi

if [ -z "$registry_id" ]
then
    echo "no env REGISTRY_ID"
    exit 0
fi

yc compute instance update-container $instance_id \
--container-name=backend \
--container-image=cr.yandex/$registry_id/comments-service:$tag \
--container-command=/app/run.sh \
--container-env-file=./backend/.env \

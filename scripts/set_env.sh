#!/bin/bash

registry_name="backend-registry"
balancer_target_group_name="backends-group"
balancer_listener_name="backend-balancer"

default_bucket_name="imosk72-comments-service"

registry_id=$(yc container registry list | grep $registry_name | awk '{split($0,a,"|"); gsub(/ /, "", a[2]); print a[2]}')
echo "set env REGISTRY_ID=$registry_id"
export REGISTRY_ID=$registry_id

target_group_id=$(yc load-balancer target-group list | grep $balancer_target_group_name | awk '{split($0,a,"|"); gsub(/ /, "", a[2]); print a[2]}')
echo "set env TARGET_GROUP_ID=$target_group_id"
export TARGET_GROUP_ID=$target_group_id

listener_address=$(yc load-balancer network-load-balancer get --name $balancer_listener_name | grep "address: " | awk '{split($0,a,": "); print a[2]}')
echo "set env LISTENER_ADDRESS=$listener_address"
export LISTENER_ADDRESS=$listener_address

echo "set env BUCKET_NAME=$default_bucket_name"
export BUCKET_NAME=$default_bucket_name

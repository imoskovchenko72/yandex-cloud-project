#!/bin/bash

registry_id=$(printenv REGISTRY_ID)
tag=$1

if [ -z "$registry_id" ]
then
    echo "no env REGISTRY_ID"
    exit 0
fi

if [ -z "$tag" ]
then
    echo "tag not specified"
    exit 0
fi

# create temporary directory to add file with version
cp -r ./backend ./tmp_backend

# add actual version
echo "BACKEND_VERSION = '$1'" > ./tmp_backend/version.py

# build docker image
docker build ./tmp_backend -t cr.yandex/$registry_id/comments-service:$1

# push docker image
docker push cr.yandex/$registry_id/comments-service:$1

# remove temporary directory
rm -rf ./tmp_backend

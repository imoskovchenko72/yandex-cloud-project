import os
import uuid
import uvicorn
from fastapi import FastAPI, Body, Response
from fastapi.middleware.cors import CORSMiddleware

from comments_repository import comments_repository
from version import BACKEND_VERSION

REPLICA_ID = f"{BACKEND_VERSION}#{uuid.uuid4()}"

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get(
    "/ping",
)
def get_comments(limit: int = 10):
    return {
        "whoami": REPLICA_ID,
    }


@app.get(
    "/comments",
)
def get_comments(limit: int = 10):
    return {
        "comments": comments_repository.select(limit=limit),
        "whoami": REPLICA_ID,
    }


@app.get(
    "/comments/{surname}",
)
def get_comments(surname: str, limit: int = 10):
    return {
        "comments": comments_repository.select_by_surname(surname=surname, limit=limit),
        "whoami": REPLICA_ID,
    }


@app.post(
    "/comments/new",
)
def create_new_comment(response: Response, name: str = Body(), surname: str = Body(), text: str = Body()):
    comments_repository.create_comment(name=name, surname=surname, text=text)
    response.status_code = 201
    return {
        "whoami": REPLICA_ID,
    }


if __name__ == "__main__":
    uvicorn.run(
        app,
        host=os.getenv("APP_HOST", "localhost"),
        port=int(os.getenv("APP_PORT", "80")),
        log_level="info",
    )

import ydb
import os


class YDBClient:
    def __init__(self):
        self.database = os.getenv("YDB_DATABASE")
        self.driver = self._create_driver()

    def _create_driver(self) -> ydb.Driver:
        driver_config = ydb.DriverConfig(
            endpoint=os.getenv("YDB_ENDPOINT"),
            database=self.database,
            credentials=ydb.credentials_from_env_variables(),
            root_certificates=ydb.load_ydb_root_certificate(),
        )

        driver = ydb.Driver(driver_config)

        try:
            driver.wait(timeout=10)
        except Exception:
            raise ValueError(driver.discovery_debug_details())

        return driver

    @property
    def table_client(self) -> ydb.TableClient:
        return self.driver.table_client

    @property
    def session_pool(self):
        return ydb.SessionPool(self.driver)

    @property
    def root(self) -> str:
        return "local"

    @property
    def full_path(self) -> str:
        return os.path.join(self.database, self.root)


ydb_client = YDBClient()

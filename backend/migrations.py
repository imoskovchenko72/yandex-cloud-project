import os
import ydb
from ydb_helpers import ydb_client


def migration1(session: ydb.Session) -> None:
    session.create_table(
        os.path.join(ydb_client.full_path, "comments"),
        ydb.TableDescription()
        .with_column(ydb.Column("id", ydb.PrimitiveType.Utf8))  # not null column
        .with_column(ydb.Column("name", ydb.OptionalType(ydb.PrimitiveType.Utf8)))
        .with_column(ydb.Column("surname", ydb.OptionalType(ydb.PrimitiveType.Utf8)))
        .with_column(ydb.Column("text", ydb.OptionalType(ydb.PrimitiveType.Utf8)))
        .with_column(ydb.Column("created_at", ydb.PrimitiveType.Datetime))
        .with_primary_key("id")
    )


if __name__ == "__main__":
    pool = ydb_client.session_pool

    pool.retry_operation_sync(migration1)

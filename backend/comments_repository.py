import datetime
import os
import uuid

import ydb

from ydb_helpers import ydb_client


class CommentsRepository:
    @property
    def table_path(self) -> str:
        return os.path.join(ydb_client.root, "comments")

    def select_by_surname(self, surname: str, limit: int = 10) -> list[dict]:
        def callee(session: ydb.Session):
            return session.transaction().execute(
                """
                SELECT *
                FROM `{}`
                WHERE `surname` = "{}"
                ORDER BY `created_at` DESC
                LIMIT {}
                """.format(self.table_path, surname, limit),
                commit_tx=True,
                settings=ydb.BaseRequestSettings().with_timeout(3).with_operation_timeout(2)
            )[0].rows

        return [dict(row) for row in ydb_client.session_pool.retry_operation_sync(callee)]

    def select(self, limit: int = 10) -> list[dict]:
        def callee(session: ydb.Session):
            return session.transaction().execute(
                """
                SELECT *
                FROM `{}`
                ORDER BY `created_at` DESC
                LIMIT {}
                """.format(self.table_path, limit),
                commit_tx=True,
                settings=ydb.BaseRequestSettings().with_timeout(3).with_operation_timeout(2)
            )[0].rows

        return [dict(row) for row in ydb_client.session_pool.retry_operation_sync(callee)]

    def create_comment(self, name: str, surname: str, text: str) -> None:
        def callee(session: ydb.Session):
            session.transaction().execute(
                """
                INSERT INTO `{}` (id, name, surname, text, created_at)
                VALUES ("{}", "{}", "{}", "{}", DATETIME("{}Z"))
                """.format(
                    self.table_path,
                    uuid.uuid4(),
                    name,
                    surname,
                    text,
                    datetime.datetime.utcnow().isoformat(timespec="seconds"),
                ),
                commit_tx=True,
                settings=ydb.BaseRequestSettings().with_timeout(3).with_operation_timeout(2)
            )

        ydb_client.session_pool.retry_operation_sync(callee)


comments_repository = CommentsRepository()
